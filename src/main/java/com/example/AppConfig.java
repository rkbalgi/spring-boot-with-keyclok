package com.example;

import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author Raghavendra Balgi
 */
@Configuration
@ComponentScan("com.example")
public class AppConfig {






  @Bean
  public AuthzClient authzClient(KeycloakSpringBootProperties kcProperties) {
    org.keycloak.authorization.client.Configuration configuration = new org.keycloak.authorization.client.Configuration(
        kcProperties.getAuthServerUrl(), kcProperties.getRealm(),
        kcProperties.getResource(), kcProperties.getCredentials(), null);


    return AuthzClient.create(configuration);
  }

}
