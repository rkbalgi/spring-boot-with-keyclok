package com.example.resources.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 *
 */
@Controller
public class OmCreateUserResources {


  @PostMapping("/om/users/create")
  public String createOmUser(Model model) {
    model.addAttribute("http_method", "POST");
    return "om_user_create";
  }

  @GetMapping("/om/users/create")
  public String createOmUserWithGET(Model model) {
    model.addAttribute("http_method", "GET");
    return "om_user_create";
  }

  @DeleteMapping("/om/users/delete")
  public String createOmDeleteWithGET(Model model) {
    model.addAttribute("http_method", "DELETE");
    return "om_del_user";
  }


}
