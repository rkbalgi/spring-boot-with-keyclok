package com.example.resources.demo;

import java.net.MalformedURLException;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 *
 */
@Controller
public class IpaCreateUserResources {


  @PostMapping("/ipa/users/create")
  public String createIpaUser(HttpServletRequest request, Model model)
      throws MalformedURLException {

    populateModel(request, model);
    model.addAttribute("http_method", "POST");
    return "ipa_user_create";
  }

  private void populateModel(HttpServletRequest request, Model model) throws MalformedURLException {

    RefreshableKeycloakSecurityContext ctx = (RefreshableKeycloakSecurityContext) request
        .getAttribute(KeycloakSecurityContext.class.getName());
    if (ctx != null) {

      String logoutUrl = ctx.getDeployment().getLogoutUrl().build().toURL().toString();
      String userName = ctx.getToken().getPreferredUsername();
      System.out.println("logout-url = " + logoutUrl);
      System.out.println("username = " + userName);
      model.addAttribute("logoutUrl", logoutUrl);
      model.addAttribute("userName", userName);

    }

  }

  @GetMapping("/ipa/users/create")
  public String createIpaUserWithGET(HttpServletRequest request, Model model)
      throws MalformedURLException {
    populateModel(request, model);
    model.addAttribute("http_method", "GET");
    return "ipa_user_create";
  }

  @DeleteMapping("/ipa/users/delete")
  @ResponseBody
  public String createIpaDeleteWithGET(HttpServletRequest request, Model model)
      throws MalformedURLException {
    System.out.println("****************************>><<<<");
    populateModel(request, model);

    model.addAttribute("http_method", "DELETE");
    return "ipa_del_user";
  }


}
