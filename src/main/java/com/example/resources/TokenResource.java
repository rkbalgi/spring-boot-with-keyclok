package com.example.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import org.keycloak.authorization.client.AuthorizationDeniedException;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.representation.TokenIntrospectionResponse;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.AuthorizationRequest.Metadata;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Raghavendra Balgi
 */
@RestController
public class TokenResource {

  private static final Logger LOG = LoggerFactory.getLogger(TokenResource.class);

  @Autowired
  private AuthzClient authzClient;

  @PostMapping("/token/generate")
  @Consumes(value = MediaType.APPLICATION_JSON_VALUE)
  public ObjectNode login(HttpServletRequest request) throws IOException {

    ObjectNode reqData = (ObjectNode) new ObjectMapper()
        .readTree(request.getInputStream());
    final String userName = reqData.get("user_name").asText();
    final String password = reqData.get("password").asText();
    AccessTokenResponse token = authzClient
        .obtainAccessToken(userName, password);

    final AuthorizationRequest authReq = new AuthorizationRequest();

    //checking for a specific permission
    authReq.setMetadata(new Metadata());
    //authReq.getMetadata().setResponseMode("decision");
    //authReq.addPermission("payroll", "write");

    AuthorizationResponse authResponse = null;
    try {
      authResponse = authzClient
          .authorization(token.getToken()).authorize(authReq);
      LOG.warn("Permission granted .. for ...");
      authReq.getPermissions().getPermissions().forEach(p -> {
        LOG.debug(p.getResourceId() + " with scopes - " + p.getScopes());
      });
    } catch (AuthorizationDeniedException e) {
      LOG.warn("Permission denied .. for ");

      authReq.getPermissions().getPermissions().forEach(p -> {
        LOG.debug(p.getResourceId() + " with scopes - " + p.getScopes());
      });
      return JsonNodeFactory.instance.objectNode().put("token", token.getToken());
    }

    LOG.info("RPT = " + authResponse.getToken());
    TokenIntrospectionResponse introspectionResponse = authzClient
        .protection()
        .introspectRequestingPartyToken(authResponse.getToken());

    introspectionResponse.getPermissions().stream().forEach(p -> {
      LOG.debug(p.getResourceName() + "-- scopes: " + p.getScopes() + " -" + p.getResourceId());
    });

    return JsonNodeFactory.instance.objectNode().put("token", token.getToken());


  }
}
