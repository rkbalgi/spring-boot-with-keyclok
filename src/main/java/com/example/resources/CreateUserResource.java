package com.example.resources;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Raghavendra Balgi
 */
//@RestController
public class CreateUserResource {

  @Autowired
  KeycloakSpringBootProperties kcProperties;


  @PostMapping("/users/create")
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectNode createUser(@RequestBody User user, HttpServletRequest request)
      throws UserNotLoggedInException {

    final ObjectNode jsonResponseObj = JsonNodeFactory.instance.objectNode();

    /*RefreshableKeycloakSecurityContext ctx = (RefreshableKeycloakSecurityContext) request
        .getAttribute(KeycloakSecurityContext.class.getName());
    if (ctx != null && ctx.isActive()) {*/

    final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
    int index = authHeader.indexOf("Bearer");

    String authToken = authHeader.substring(index + 6).trim();

    System.out.println("Auth Token = " + authToken);

    Keycloak keycloak = Keycloak
        .getInstance(kcProperties.getAuthServerUrl(), kcProperties.getRealm(),
            kcProperties.getResource(),
            authToken);

    UsersResource usersResource = keycloak
        .realm(kcProperties.getRealm()).users();

    UserRepresentation userRep = buildUserRep(user);

    Response response = usersResource.create(userRep);
    if (response.getStatus() == HttpStatus.SC_OK) {
      return jsonResponseObj.put("status", "OK");
    } else {
      return jsonResponseObj.put("status", "Failed with code - " + response.getStatus());
    }

    /*} else {
      throw new UserNotLoggedInException();
    }*/
  }

  private UserRepresentation buildUserRep(User user) {

    UserRepresentation userRep = new UserRepresentation();
    userRep.setUsername(user.getUserName());
    userRep.setFirstName(user.getFirstName());
    userRep.setLastName(user.getLastName());
    userRep.setEnabled(true);
    userRep.setEmail(user.getUserName()+"@example.com");


    final CredentialRepresentation userPassword = new CredentialRepresentation();
    userPassword.setValue("password123");
    userPassword.setTemporary(false);
    userPassword.setType(CredentialRepresentation.PASSWORD);
    userRep.setCredentials(List.of(userPassword));


    return userRep;


  }


}
