package com.example.resources;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.AdapterUtils;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Raghavendra Balgi
 */
@RestController
public class HelloResource {


  private static final Logger LOG = LoggerFactory.getLogger(HelloResource.class);

  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String hello(HttpServletRequest request) {

    System.out.println(request.getUserPrincipal());

    RefreshableKeycloakSecurityContext ctx = (RefreshableKeycloakSecurityContext) request
        .getAttribute(KeycloakSecurityContext.class.getName());
    System.out.println("Token = " + ctx.getTokenString());
    System.out.println("***" + ctx.getToken().getPreferredUsername());

    Set<String> roles = AdapterUtils.getRolesFromSecurityContext(ctx);

    roles.forEach(System.out::println);

    return "Hello World";
  }

}
