package com.example;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import org.junit.Test;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.representations.AccessTokenResponse;

/**
 * Unit test for simple App.
 */
public class AppTest {

  /**
   * Rigorous Test :-)
   */
  @Test
  public void shouldAnswerWithTrue() {
    assertTrue(true);
  }


  @Test
  public void generateToken() {

    HashMap<String, Object> credsMap = new HashMap<>();
    credsMap.put("secret", "71df4a13-7f58-45be-9f8a-9abac88f97fe");

    Configuration configuration = new Configuration(
        "http://localhost:8080/auth", "infinx-demo",
        "identiti-app", credsMap, null);

    AuthzClient authzClient = AuthzClient.create(configuration);
    AccessTokenResponse token = authzClient
        .obtainAccessToken("admin_user", "password");
    System.out.println(token.getToken());

  }
}
